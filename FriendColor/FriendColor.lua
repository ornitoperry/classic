function FriendColor_ClassColor(class)
	local localClass;
	
	if GetLocale() ~= "enUS" then
		for k,v in pairs(LOCALIZED_CLASS_NAMES_FEMALE) do if class == v then localClass = k end end
	else
		for k,v in pairs(LOCALIZED_CLASS_NAMES_MALE) do if class == v then localClass = k end end
	end
	
	local classColor = (CUSTOM_CLASS_COLORS or RAID_CLASS_COLORS)[localClass];
	return classColor; -- check nil
end

function FriendColor_BNetFriend(i, friendOffset)
	local bnetIDAccount, accountName, battleTag, isBattleTagPresence, characterName, bnetIDGameAccount, client, isOnline, lastOnline, isAFK, isDND, messageText, noteText, isRIDFriend, messageTime, canSoR, isReferAFriend, canSummonFriend = BNGetFriendInfo(i);
	
	if isOnline == false then
		return;
	end
	
	if client ~= BNET_CLIENT_WOW then
		return;
	end
	
	local hasFocus, characterName, client, realmName, realmID, faction, race, class, guild, zoneName, level, gameText, broadcastText, broadcastTime, canSoR, toonID, bnetIDAccount, isGameAFK, isGameBusy = BNGetGameAccountInfo(bnetIDGameAccount);
	
	local classc = FriendColor_ClassColor(class);
	if not classc then
		return;
	end
	
	local index = i-friendOffset;
	local nameString = _G["FriendsFrameFriendsScrollFrameButton"..(index).."Name"];
	if nameString then
		nameString:SetText(accountName.." ("..characterName..", L"..level..")");
		nameString:SetTextColor(classc.r, classc.g, classc.b);
	end
	
	if CanCooperateWithGameAccount(toonID) ~= true then
		local nameString = _G["FriendsFrameFriendsScrollFrameButton"..(index).."Info"];
		if nameString then
			nameString:SetText(zoneName.." ("..realmName..")");
		end
	end
end

function FriendColor_Friend(i, friendOffset, BNetOffset)
	local friendInfo = C_FriendList.GetFriendInfoByIndex(i);
		
	if friendInfo.connected == false then
		return;
	end
	
	local classc = FriendColor_ClassColor(friendInfo.className);
	if not classc then
		return;
	end
	
	local index = i+BNetOffset-friendOffset;
	
	local nameString = _G["FriendsFrameFriendsScrollFrameButton"..(index).."Name"];
	if nameString and friendInfo.name then
		nameString:SetText(friendInfo.name..", L"..friendInfo.level);
		nameString:SetTextColor(classc.r, classc.g, classc.b);
	end
end

function FriendColor_GetFriendOffset()
	local friendOffset = HybridScrollFrame_GetOffset(FriendsFrameFriendsScrollFrame);
	if not friendOffset then
		return;
	end
	if friendOffset < 0 then
		friendOffset = 0;
	end
	
	return friendOffset;
end

function FriendColor_Hook_FriendsList_Update()
	local friendOffset = FriendColor_GetFriendOffset();
	local numBNetTotal, numBNetOnline = BNGetNumFriends();
	
	-- Online Battlenet friends
	if numBNetOnline > 0 then
		for i=1, numBNetTotal, 1 do
			FriendColor_BNetFriend(i, friendOffset);
		end
	end

	-- Online WoW friends
	local numFriends = C_FriendList.GetNumFriends();
	local numOnline = C_FriendList.GetNumOnlineFriends();
	
	if numOnline > 0 then
	local BNetOffset = numBNetOnline;
		for i=1, numOnline, 1 do
			FriendColor_Friend(i, friendOffset, BNetOffset);
		end
	end
end;

hooksecurefunc("FriendsList_Update", FriendColor_Hook_FriendsList_Update);
hooksecurefunc("HybridScrollFrame_Update", FriendColor_Hook_FriendsList_Update);
