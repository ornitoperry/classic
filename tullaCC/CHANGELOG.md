# tullaCC

## [8.2.5](https://github.com/tullamods/tullaCC/tree/8.2.5) (2019-09-24)
[Full Changelog](https://github.com/tullamods/tullaCC/compare/8.2.3...8.2.5)

- updated toc for 8.2.5  
