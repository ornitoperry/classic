# AtlasLootClassic

This mod is distributed under Version 2 of the GPL.  A copy of the GPL is included in this zip file with links to non-english translations.

[Changelog history](https://github.com/Hoizame/AtlasLootClassic/blob/master/AtlasLootClassic/Documentation/Release_Notes.md)

## v1.2.2 (Oct. 02, 2019)

- Update BRD/LBRS loot
- Add much more drop rate data from classic.wowhead.com. Use their tool/addon and help them to get more data!
