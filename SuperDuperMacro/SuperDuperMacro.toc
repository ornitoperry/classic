# SuperDuperMacro has most recently been tested against:
#   Version 8.1.0 (29088) (Release x64)
#   Jan 16 2019

# The interface number can be learned with:
#   /dump select( 4, GetBuildInfo() )
## Interface:             80100
## Title:                 Super Duper Macro

## Notes:                 Enables the creation of incredibly long macros.
## Author:                spiralofhope, hypehuman
## Version:               8.1.0.2
## X-Date:                2019-01-16
## X-Website:             https://github.com/spiralofhope/SuperDuperMacro/
## X-Feedback:            https://github.com/spiralofhope/SuperDuperMacro/issues/new


## SavedVariables:        sdm_version, sdm_listFilters, sdm_iconSize, sdm_mainContents, sdm_macros
## DefaultState:          enabled











SuperDuperMacro_Core.lua
SuperDuperMacro_Interface.lua
SuperDuperMacro_Sharing.lua
SuperDuperMacro_Frames.lua
