# Plexus

## [v1.7.1](https://github.com/doadin/Plexus/tree/v1.7.1) (2019-10-08)
[Full Changelog](https://github.com/doadin/Plexus/compare/v1.6.9...v1.7.1)

- [Statuses\] Lower a Few Default Priorities, Leave Room to Grow  
