--[[--------------------------------------------------------------------
	Plexus
	Compact party and raid unit frames.
	Copyright (c) 2006-2009 Kyle Smith (Pastamancer)
	Copyright (c) 2009-2018 Phanx <addons@phanx.net>
	All rights reserved. See the accompanying LICENSE file for details.
------------------------------------------------------------------------
	PlexusLocale-frFR.lua
	French localization
	Contributors: brubru777, Devfool, Matisk, NoGynGz, Pettigrow, Strigx, trasher
----------------------------------------------------------------------]]

if GetLocale() ~= "frFR" then return end

local _, Plexus = ...
local L = { }
Plexus.L = L

------------------------------------------------------------------------
--	PlexusCore


------------------------------------------------------------------------
--	PlexusFrame


------------------------------------------------------------------------
--	PlexusLayout


------------------------------------------------------------------------
--	PlexusLayoutLayouts


------------------------------------------------------------------------
--	PlexusLDB


------------------------------------------------------------------------
--	PlexusStatus


------------------------------------------------------------------------
--	PlexusStatusAbsorbs


------------------------------------------------------------------------
--	PlexusStatusAggro


------------------------------------------------------------------------
--	PlexusStatusAuras


------------------------------------------------------------------------
--	PlexusStatusHeals


------------------------------------------------------------------------
--	PlexusStatusHealth


------------------------------------------------------------------------
--	PlexusStatusMana


------------------------------------------------------------------------
--	PlexusStatusName


------------------------------------------------------------------------
--	PlexusStatusRange


------------------------------------------------------------------------
--	PlexusStatusReadyCheck


------------------------------------------------------------------------
--	PlexusStatusResurrect


------------------------------------------------------------------------
--	PlexusStatusTarget


------------------------------------------------------------------------
--	PlexusStatusVehicle


------------------------------------------------------------------------
--	PlexusStatusVoiceComm


